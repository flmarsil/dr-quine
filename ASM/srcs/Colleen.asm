; quine self replication

section .rodata
data db '; quine self replication%1$c%1$csection .rodata%1$cdata db %2$c%3$s%2$c, 0%1$c%1$csection .text%1$cextern printf%1$cglobal main%1$c%1$cfunction:%1$clea rdi, [rel data]%1$cmov rsi, 10%1$cmov rdx, 39%1$clea rcx, [rel data]%1$cxor rax, rax%1$ccall printf%1$cjmp end%1$c%1$cmain:%1$c; author : flmarsil%1$ccall function%1$c%1$cend:%1$cmov rax, 60%1$cxor rdi, rdi%1$csyscall%1$c', 0

section .text
extern printf
global main

function:
lea rdi, [rel data]
mov rsi, 10
mov rdx, 39
lea rcx, [rel data]
xor rax, rax
call printf
jmp end

main:
; author : flmarsil
call function

end:
mov rax, 60
xor rdi, rdi
syscall
