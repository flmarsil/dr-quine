bits 64

%define hello 'Hello World !'

segment .data
    message db hello, 10        ; ascii for \n
    message_len equ $- message

segment .text
    global _start 

_start:
    mov rax, 1                  ; system call write
    mov rdi, 1                  ; stdout
    mov rsi, message            ; message content
    mov rdx, message_len + 1    ; content lenght
    syscall

    mov rax, 60                 ; exit
    mov rdi, 0
    syscall