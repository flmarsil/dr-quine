#include <stdio.h>

/*
	quine self replication
*/

char* function() {
	char* data = "#include <stdio.h>%1$c%1$c/*%1$c%2$cquine self replication%1$c*/%1$c%1$cchar* function() {%1$c%2$cchar* data = %3$c%4$s%3$c;%1$c%2$creturn (data);%1$c}%1$c%1$cint main() {%1$c%2$c/*%1$c%2$c%2$cauthor : flmarsil%1$c%2$c*/%1$c%2$cprintf(function(), 10, 9, 34, function());%1$c%2$creturn (0);%1$c}%1$c";
	return (data);
}

int main() {
	/*
		author : flmarsil
	*/
	printf(function(), 10, 9, 34, function());
	return (0);
}
